// Code generated by protoc-gen-go.
// source: exampleMessage.proto
// DO NOT EDIT!

/*
Package exampleMessage is a generated protocol buffer package.

It is generated from these files:
	exampleMessage.proto

It has these top-level messages:
	HelloRequest
	HelloReply
*/
package exampleMessage

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type HelloRequest struct {
	Name string `protobuf:"bytes,1,opt,name=name" json:"name,omitempty"`
	A    int32  `protobuf:"varint,2,opt,name=a" json:"a,omitempty"`
	B    int32  `protobuf:"varint,3,opt,name=b" json:"b,omitempty"`
}

func (m *HelloRequest) Reset()                    { *m = HelloRequest{} }
func (m *HelloRequest) String() string            { return proto.CompactTextString(m) }
func (*HelloRequest) ProtoMessage()               {}
func (*HelloRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *HelloRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *HelloRequest) GetA() int32 {
	if m != nil {
		return m.A
	}
	return 0
}

func (m *HelloRequest) GetB() int32 {
	if m != nil {
		return m.B
	}
	return 0
}

type HelloReply struct {
	Message string `protobuf:"bytes,1,opt,name=message" json:"message,omitempty"`
	Result  int32  `protobuf:"varint,2,opt,name=result" json:"result,omitempty"`
}

func (m *HelloReply) Reset()                    { *m = HelloReply{} }
func (m *HelloReply) String() string            { return proto.CompactTextString(m) }
func (*HelloReply) ProtoMessage()               {}
func (*HelloReply) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *HelloReply) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *HelloReply) GetResult() int32 {
	if m != nil {
		return m.Result
	}
	return 0
}

func init() {
	proto.RegisterType((*HelloRequest)(nil), "exampleMessage.HelloRequest")
	proto.RegisterType((*HelloReply)(nil), "exampleMessage.HelloReply")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for Greeter service

type GreeterClient interface {
	SayHello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloReply, error)
}

type greeterClient struct {
	cc *grpc.ClientConn
}

func NewGreeterClient(cc *grpc.ClientConn) GreeterClient {
	return &greeterClient{cc}
}

func (c *greeterClient) SayHello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloReply, error) {
	out := new(HelloReply)
	err := grpc.Invoke(ctx, "/exampleMessage.greeter/SayHello", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Greeter service

type GreeterServer interface {
	SayHello(context.Context, *HelloRequest) (*HelloReply, error)
}

func RegisterGreeterServer(s *grpc.Server, srv GreeterServer) {
	s.RegisterService(&_Greeter_serviceDesc, srv)
}

func _Greeter_SayHello_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HelloRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GreeterServer).SayHello(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/exampleMessage.greeter/SayHello",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GreeterServer).SayHello(ctx, req.(*HelloRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Greeter_serviceDesc = grpc.ServiceDesc{
	ServiceName: "exampleMessage.greeter",
	HandlerType: (*GreeterServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SayHello",
			Handler:    _Greeter_SayHello_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "exampleMessage.proto",
}

func init() { proto.RegisterFile("exampleMessage.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 206 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x49, 0xad, 0x48, 0xcc,
	0x2d, 0xc8, 0x49, 0xf5, 0x4d, 0x2d, 0x2e, 0x4e, 0x4c, 0x4f, 0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9,
	0x17, 0xe2, 0x43, 0x15, 0x55, 0xb2, 0xe3, 0xe2, 0xf1, 0x48, 0xcd, 0xc9, 0xc9, 0x0f, 0x4a, 0x2d,
	0x2c, 0x4d, 0x2d, 0x2e, 0x11, 0x12, 0xe2, 0x62, 0xc9, 0x4b, 0xcc, 0x4d, 0x95, 0x60, 0x54, 0x60,
	0xd4, 0xe0, 0x0c, 0x02, 0xb3, 0x85, 0x78, 0xb8, 0x18, 0x13, 0x25, 0x98, 0x14, 0x18, 0x35, 0x58,
	0x83, 0x18, 0x13, 0x41, 0xbc, 0x24, 0x09, 0x66, 0x08, 0x2f, 0x49, 0xc9, 0x8e, 0x8b, 0x0b, 0xaa,
	0xbf, 0x20, 0xa7, 0x52, 0x48, 0x82, 0x8b, 0x3d, 0x17, 0x62, 0x30, 0xd4, 0x00, 0x18, 0x57, 0x48,
	0x8c, 0x8b, 0xad, 0x28, 0xb5, 0xb8, 0x34, 0xa7, 0x04, 0x6a, 0x10, 0x94, 0x67, 0x14, 0xc8, 0xc5,
	0x9e, 0x5e, 0x94, 0x9a, 0x5a, 0x92, 0x5a, 0x24, 0xe4, 0xc6, 0xc5, 0x11, 0x9c, 0x58, 0x09, 0x36,
	0x4d, 0x48, 0x46, 0x0f, 0xcd, 0xf5, 0xc8, 0x8e, 0x94, 0x92, 0xc2, 0x21, 0x5b, 0x90, 0x53, 0xa9,
	0xc4, 0xe0, 0x64, 0xc1, 0x25, 0x9f, 0x99, 0xaf, 0x97, 0x5e, 0x54, 0x90, 0x0c, 0x53, 0x56, 0x8c,
	0xa6, 0xde, 0x49, 0x18, 0x95, 0x1f, 0x00, 0x0a, 0x9a, 0x00, 0xc6, 0x24, 0x36, 0x70, 0x18, 0x19,
	0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0x10, 0x3f, 0x9b, 0xe2, 0x3b, 0x01, 0x00, 0x00,
}
